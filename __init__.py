# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from safety_calendar import SafetyCalendar

def register():
    Pool.register(
        SafetyCalendar,
        module='safety_calendar', type_='model')
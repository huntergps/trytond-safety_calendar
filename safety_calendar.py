# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import datetime

from trytond.model import ModelSQL, ModelView, fields, sequence_ordered
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond import backend

__all__ = ['SafetyCalendar']

class SafetyCalendar(ModelSQL, ModelView):
    'Safety Calendar'
    __name__ = "activity.safetycalendar"

    #code = fields.Char('Code', readonly=True, select=True)
    subject = fields.Char('Subject')
    ext_id = fields.Char('External Id', readonly=True,
        required=False, select=True)    
    dtstart = fields.DateTime('Date', required=True, select=True)
    dtend = fields.DateTime('End Date', select=True)
    type = fields.Selection([
            ('driver', 'Driver'),
            ('equipm', 'Equipmen'),
            ], 'Type', required=True)
    type_document = fields.Selection([
            ('twicdt', 'Twic Expiration'),
            ('seaexp', 'Sealink  Expiration'),
            ('mecdate', 'Medical C Exp Date'),
            ('licexp', 'License expiration Expiration'),
            ('drgtst', 'Drug test date'),
            ('myrdat', 'Mvr Exp.date'),
            ('regdat', 'Registration date'),
            ('lnsdat', 'Last Inspection Date'),
            ('ifta', 'IFTA'),
            ('huitdat', 'HUT date'), 
            ('expins', 'Exp. insurance Date'),
            ('bobins', 'Bobtail  Exp. date'), 
            ('liains', 'Liability Exp. date'),
            ('smkinp', 'Smoke Inspection Exp.'),                                                                         
            ], 'Document', required=True)    
    description = fields.Text('Description')
    

    @classmethod
    def __setup__(cls):
        super(SafetyCalendar, cls).__setup__()
        cls._order.insert(0, ('dtstart', 'ASC'))
        cls._order.insert(1, ('subject', 'DESC'))
        


    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('subject',) + tuple(clause[1:]),
            ('type_document',) + tuple(clause[1:]),
            ]

    @staticmethod
    def default_dtstart():
        return datetime.datetime.now()

    @staticmethod
    def default_type():
        return 'driver'

    @staticmethod
    def default_type_document():
        return 'licexp'
